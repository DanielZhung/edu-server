package cn.zxd.education;

import cn.zxd.education.manager.ConfigManager;
import cn.zxd.education.util.AVDeviceUtil;
import com.avos.avoscloud.AVOSCloud;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class EducationApplication {

    public static void main(String[] args) {
        AVOSCloud.initialize("1j3OylkPSes1PwYeUoFmk3yu-gzGzoHsz", "7n1od1bAN55d66nDrfeyHjMl", "EO1XbNbpTOSBG10pQl0Utu4n");
        AVDeviceUtil.offlineAllDevice(ConfigManager.getInstance().getSchoolIndex());
        SpringApplication.run(EducationApplication.class, args);
    }
}
