package cn.zxd.education.service;

import cn.zxd.education.bean.ByteMessage;
import cn.zxd.education.bean.Device;
import cn.zxd.education.manager.DeviceManager;
import cn.zxd.education.util.ByteUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timer;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class NettyHandler extends SimpleChannelInboundHandler<ByteMessage> {

    Timer heartBeatCountTimer = null;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        LoggerFactory.getLogger(NettyHandler.class).info("Client Connected: " + ctx.channel().remoteAddress().toString());
        heartBeatCountTimer = new HashedWheelTimer();
        heartBeatCountTimer.newTimeout(timeout -> {
            LoggerFactory.getLogger(NettyHandler.class).error(ctx.channel().remoteAddress().toString() + " No Register, CLOSE connection");
            closeChannel(ctx);
        }, 30, TimeUnit.SECONDS);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        DeviceManager.getInstance().deleteDeviceByContext(ctx);
        LoggerFactory.getLogger(NettyHandler.class).error("Client Disconnect: " + ctx.channel().remoteAddress().toString());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteMessage msg) {
        switch (msg.getType()) {
            case ByteMessage.ByteMessageTypeRegister: {
                LoggerFactory.getLogger(NettyHandler.class).info(ctx.channel().remoteAddress().toString() + " Register Message: " + msg.getContent() + ", START Timer...");
                if (heartBeatCountTimer != null) {
                    LoggerFactory.getLogger(NettyHandler.class).info(ctx.channel().remoteAddress().toString() + "heartBeatCountTimer not null");
                    heartBeatCountTimer.stop();
                    heartBeatCountTimer = null;
                }
                heartBeatCountTimer = new HashedWheelTimer();
                heartBeatCountTimer.newTimeout(timeout -> {
                    LoggerFactory.getLogger(NettyHandler.class).error(ctx.channel().remoteAddress().toString() + " NO HeartBeat, CLOSE connection");
                    closeChannel(ctx);
                }, 60, TimeUnit.SECONDS);
                String deviceName = new String(ByteUtil.hexString2ByteArray(msg.getContent()));
                boolean addDevice = DeviceManager.getInstance().addDevice(deviceName, ctx);
                ctx.writeAndFlush(addDevice ? ByteMessage.RegisterSuccess : ByteMessage.RegisterFail);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ByteMessage request = new ByteMessage(ByteMessage.ByteMessageTypeIORequest, "01 00");
                ctx.writeAndFlush(request);
            }
                break;
            case ByteMessage.ByteMessageTypeHeartBeat: {
                LoggerFactory.getLogger(NettyHandler.class).info(ctx.channel().remoteAddress().toString() + " HeartBeat Message, RECOUNT");
                Device device = DeviceManager.getInstance().getDeviceByContext(ctx);
                if (device == null) {
                    LoggerFactory.getLogger(NettyHandler.class).error(ctx.channel().remoteAddress().toString() + " NOT REGISTER, don't response");
                    return;
                }
                if (heartBeatCountTimer != null) {
                    LoggerFactory.getLogger(NettyHandler.class).info(ctx.channel().remoteAddress().toString() + "heartBeatCountTimer not null");
                    heartBeatCountTimer.stop();
                    heartBeatCountTimer = null;
                }
                heartBeatCountTimer = new HashedWheelTimer();
                heartBeatCountTimer.newTimeout(timeout -> {
                    LoggerFactory.getLogger(NettyHandler.class).error(ctx.channel().remoteAddress().toString() + " HeartBeat TimeOut, CLOSE connection");
                    closeChannel(ctx);
                }, 60, TimeUnit.SECONDS);
                ctx.writeAndFlush(ByteMessage.HeartBeatResponse);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ByteMessage request = new ByteMessage(ByteMessage.ByteMessageTypeIORequest, "01 00");
                ctx.writeAndFlush(request);
            }
            break;
            case ByteMessage.ByteMessageTypeIO: {
                LoggerFactory.getLogger(NettyHandler.class).info(ctx.channel().remoteAddress().toString() + "Device IO");
                byte[] data = ByteUtil.hexString2ByteArray(msg.getContent());
                if (data[0] == 0x01) {//输入
                    Device device = DeviceManager.getInstance().getDeviceByContext(ctx);
                    if (device != null) {
                        device.setPowerOn(data[1] == 0);
                    }
                } else if (data[0] == 0x02) {

                } else {

                }
            }
            break;
            case ByteMessage.ByteMessageTypeCMD:
                break;
            case ByteMessage.ByteMessageTypeUnknown:
            default:
                break;
        }
    }

    private void closeChannel(ChannelHandlerContext ctx) {
        ctx.close();
    }
}
