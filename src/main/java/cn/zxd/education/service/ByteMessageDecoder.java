package cn.zxd.education.service;

import cn.zxd.education.bean.ByteMessage;
import cn.zxd.education.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class ByteMessageDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int readableLength = byteBuf.readableBytes();
        if (readableLength > ByteUtil.EXTRA_LENGTH) {
            if (readableLength >= ByteUtil.TOO_MAX_LENGTH) {
                byteBuf.skipBytes(byteBuf.readableBytes());
                LoggerFactory.getLogger(ByteToMessageDecoder.class).error("Error Message Length too LONG:" + byteBuf.readableBytes());
                return;
            }
            byte prefix = byteBuf.readByte();
            if (prefix != ByteUtil.BYTE_PREFIX) {
                //LoggerFactory.getLogger(ByteToMessageDecoder.class).error(String.format("Error Message PREFIX: %02x", prefix & 0xff));
                return;
            }
            int length = byteBuf.readByte();
            if (length != readableLength - ByteUtil.EXTRA_LENGTH + 1) {
                LoggerFactory.getLogger(ByteToMessageDecoder.class).error("Error Message Length:" + length);
                return;
            } else {
                LoggerFactory.getLogger(ByteToMessageDecoder.class).debug("Message Length:" + length);
            }
            byte[] content = new byte[length];
            byteBuf.readBytes(content);
            long sum = 0;
            for (int i = 0; i < length; i++) {
                sum += content[i] >= 0 ? content[i] : (content[i] + ByteUtil.BYTE_TO_CHAR);
            }
            byte getSumHigh = byteBuf.readByte();
            byte getSumLow = byteBuf.readByte();
            long getSum = (getSumHigh >= 0 ? getSumHigh : (getSumHigh + ByteUtil.BYTE_TO_CHAR)) * ByteUtil.BYTE_TO_CHAR + (getSumLow >= 0 ? getSumLow : (getSumLow + ByteUtil.BYTE_TO_CHAR));
            if (sum != getSum) {
                LoggerFactory.getLogger(ByteToMessageDecoder.class).error("Error Message CHECK_SUM: get:" + getSum + " check:" + sum);
                return;
            }
            LoggerFactory.getLogger(ByteToMessageDecoder.class).info("Message : 55 " + String.format("%02x ", length) + ByteUtil.byte2HexString(content) + String.format(" %02X %02X", (getSumHigh >= 0 ? getSumHigh : (getSumHigh + ByteUtil.BYTE_TO_CHAR)), (getSumLow >= 0 ? getSumLow : (getSumLow + ByteUtil.BYTE_TO_CHAR))));
            ByteMessage message = new ByteMessage(content[0], ByteUtil.byte2HexString(content, 2));
            list.add(message);
        } else {
            LoggerFactory.getLogger(ByteToMessageDecoder.class).error("Error Message Length too SHORT:" + byteBuf.readableBytes());
        }

    }

}
