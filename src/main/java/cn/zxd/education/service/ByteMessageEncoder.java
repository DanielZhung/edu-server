package cn.zxd.education.service;

import cn.zxd.education.bean.ByteMessage;
import cn.zxd.education.util.ByteUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.LoggerFactory;

public class ByteMessageEncoder extends MessageToByteEncoder<ByteMessage> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, ByteMessage byteMessage, ByteBuf byteBuf) throws Exception {
        byte[] sendValue = packageProtocol(byteMessage);
        LoggerFactory.getLogger(ByteMessageEncoder.class).info(channelHandlerContext.channel().remoteAddress().toString() + " Send Message: " + ByteUtil.byte2HexString(sendValue));
        byteBuf.writeBytes(sendValue);
    }

    private static byte[] packageProtocol(ByteMessage byteMessage) {
        int type = byteMessage.getType();
        String content = byteMessage.getContent();
        int contentLength = 0;

        byte[][] contentBytes = new byte[1][];
        //计算协议
        for (int i = 0; i < 1; i++) {
            contentBytes[i] = ByteUtil.hexString2ByteArray(content);
            contentLength += contentBytes[i].length + 1;
        }

        int totalLength = 1 + 1 + 1 + contentLength + 2;// 0x55 长度 协议关键字  内容长度 校验
        byte[] sendBytes = new byte[totalLength];
        sendBytes[0] = 0x55;
        sendBytes[1] = (byte) (contentLength + 1);
        sendBytes[2] = (byte) type;
        int index = 3;
        long sum = type;
        for (int i = 0; i < contentBytes.length; i++) {
            sendBytes[index++] = (byte) contentBytes[i].length;
            sum += contentBytes[i].length;
            for (int j = 0; j < contentBytes[i].length; j++) {
                sendBytes[index++] = contentBytes[i][j];
                sum += contentBytes[i][j] >= 0 ? contentBytes[i][j] : (contentBytes[i][j] + 256);
            }
        }
        sendBytes[index++] = (byte) (sum / 256);
        sendBytes[index++] = (byte) (sum % 256);
        return sendBytes;
    }

}
