package cn.zxd.education.service;

import cn.zxd.education.bean.NettyConfig;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
public class NettyServer {

    @Autowired
    NettyConfig nettyConfig;

    private EventLoopGroup boss = new NioEventLoopGroup();
    private EventLoopGroup worker = new NioEventLoopGroup();
    private ServerBootstrap bootstrap = new ServerBootstrap();

    private ChannelFuture channelFuture = null;

    @PostConstruct
    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    bootstrap.group(boss, worker).channel(NioServerSocketChannel.class)
                            .handler(new LoggingHandler(LogLevel.INFO))
                            .option(ChannelOption.SO_BACKLOG, 1024)
                            .childOption(ChannelOption.SO_KEEPALIVE, true)
                            .childHandler(new ChannelInitializer<SocketChannel>() {
                                @Override
                                protected void initChannel(SocketChannel ch) throws Exception {
                                    ChannelPipeline channelPipeline = ch.pipeline();
                                    channelPipeline.addLast("decode", new ByteMessageDecoder());
                                    channelPipeline.addLast("encode", new ByteMessageEncoder());
                                    channelPipeline.addLast("handler", new NettyHandler());
                                }
                            });
                    channelFuture = bootstrap.bind(nettyConfig.getPort()).sync();
                    LoggerFactory.getLogger(NettyServer.class).info("Netty start at " + nettyConfig.getPort());
                    channelFuture.channel().closeFuture().sync();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    worker.shutdownGracefully();
                    boss.shutdownGracefully();
                }
            }
        }).start();
    }

    @PreDestroy
    public void stop() {
        channelFuture.awaitUninterruptibly();
        worker.shutdownGracefully();
        boss.shutdownGracefully();
    }


}
