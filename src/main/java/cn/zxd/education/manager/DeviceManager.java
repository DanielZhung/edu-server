package cn.zxd.education.manager;

import cn.zxd.education.bean.ByteMessage;
import cn.zxd.education.bean.Device;
import cn.zxd.education.util.AVDeviceUtil;
import io.netty.channel.ChannelHandlerContext;

import java.util.ArrayList;
import java.util.List;

public class DeviceManager {

    private static DeviceManager ourInstance = null;
    private static final Object locker = new Object();

    private List<Device> deviceList = null;

    public static DeviceManager getInstance() {
        if (ourInstance == null) {
            synchronized (locker) {
                if (ourInstance == null) {
                    ourInstance = new DeviceManager();
                }
            }
        }
        return ourInstance;
    }

    private DeviceManager() {
        deviceList = new ArrayList<>();
    }

    public boolean hasDevice(String name) {
        return true;
    }

    public boolean addDevice(String name, ChannelHandlerContext context) {
        Device device = getDeviceByName(name);
        if (device == null) {
            Device newDevice = new Device(name, context);
            synchronized (locker) {
                AVDeviceUtil.AddAndOnlineDevice(ConfigManager.getInstance().getSchoolIndex(), newDevice);
                deviceList.add(newDevice);
            }
            return true;
        } else {
            return false;
        }
    }

    public Device getDeviceByName(String name) {
        if (name != null && name.length() > 0) {
            synchronized (locker) {
                Device device = null;
                for (Device device1 : deviceList) {
                    if (device1.getRegisterName().equalsIgnoreCase(name)) {
                        device = device1;
                        break;
                    }
                }
                return device;
            }
        } else {
            return null;
        }
    }

    public Device getDeviceByContext(ChannelHandlerContext context) {
        if (context != null) {
            synchronized (locker) {
                Device device = null;
                for (Device device1 : deviceList) {
                    if (device1.getContext() == context) {
                        device = device1;
                        break;
                    }
                }
                return device;
            }
        } else {
            return null;
        }
    }

    public void deleteDeviceByName(String name) {
        Device device = getDeviceByName(name);
        if (device != null) {
            synchronized (locker) {
                AVDeviceUtil.offlineDevice(ConfigManager.getInstance().getSchoolIndex(), device);
                deviceList.remove(device);
            }
        }
    }

    public void deleteDeviceByContext(ChannelHandlerContext context) {
        Device device = getDeviceByContext(context);
        if (device != null) {
            synchronized (locker) {
                AVDeviceUtil.offlineDevice(ConfigManager.getInstance().getSchoolIndex(), device);
                deviceList.remove(device);
            }
        }
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    public boolean sendMessageByName(String name, ByteMessage message) {
        Device device = getDeviceByName(name);
        if (device != null) {
            device.getContext().writeAndFlush(message);
            return true;
        } else {
            return false;
        }
    }
}
