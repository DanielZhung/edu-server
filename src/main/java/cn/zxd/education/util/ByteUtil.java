package cn.zxd.education.util;

public class ByteUtil {

    public static final byte BYTE_PREFIX = 0x55;

    public static final int EXTRA_LENGTH = 5;

    public static final int BYTE_TO_CHAR = 256;

    public static final int TOO_MAX_LENGTH = 2048;

    public static String byte2HexString(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 3);
        for (byte b : bytes) { // 使用String的format方法进行转换
            buf.append(String.format(" %02X", b & 0xff));
        }
        if (buf.length() > 1)
            buf.deleteCharAt(0);
        return buf.toString();
    }

    public static String byte2HexString(byte[] bytes, int start) {
        byte[] src = new byte[bytes.length - start];
        System.arraycopy(bytes, start, src, 0, src.length);
        return byte2HexString(src);
    }

    public static byte[] hexString2ByteArray(String hexStr) {
        if (hexStr == null || hexStr.trim().equals("")) {
            return new byte[0];
        }
        String str = hexStr.trim().replace(" ", "");

        byte[] bytes = new byte[str.length() / 2];
        for(int i = 0; i < str.length() / 2; i++) {
            String subStr = str.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }

        return bytes;
    }

}
