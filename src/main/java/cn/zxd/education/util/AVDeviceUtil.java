package cn.zxd.education.util;

import cn.zxd.education.bean.Device;
import com.avos.avoscloud.*;

import java.util.ArrayList;
import java.util.List;

public class AVDeviceUtil {


    public static void offlineDevice(int schoolIndex, Device device) {
        AVQuery<AVObject> deviceQuery = new AVQuery<>("Device");
        deviceQuery.whereEqualTo("Name", device.getRegisterName());
        deviceQuery.whereEqualTo("SchoolIndex", schoolIndex);
        List<AVObject> response = null;
        try {
            response = deviceQuery.find();
            if (response != null && response.size() > 0) {
                AVObject object = response.get(0);
                object.put("Online", false);
                object.save();
            }
        } catch (AVException e) {
            e.printStackTrace();
        }
    }

    public static void offlineAllDevice(int schoolIndex) {
        AVQuery<AVObject> deviceQuery = new AVQuery<>("Device");
        deviceQuery.whereEqualTo("SchoolIndex", schoolIndex);
        deviceQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                for (AVObject object : list) {
                    object.put("Online", false);
                }
                AVObject.saveAllInBackground(list);
            }
        });
    }

    public static void AddAndOnlineDevice(int schoolIndex, Device device) {
        AVQuery<AVObject> deviceQuery = new AVQuery<>("Device");
        deviceQuery.whereEqualTo("Name", device.getRegisterName());
        deviceQuery.whereEqualTo("SchoolIndex", schoolIndex);
        List<AVObject> response = null;
        try {
            response = deviceQuery.find();
            if (response == null || response.size() < 1) {
                AVObject object = new AVObject("Device");
                object.put("Name", device.getRegisterName());
                object.put("SchoolIndex", schoolIndex);
                object.put("Online", true);
                object.save();
            } else {
                AVObject object = response.get(0);
                object.put("Online", true);
                object.save();
            }
        } catch (AVException e) {
            e.printStackTrace();
        }
    }
}
