package cn.zxd.education.controller;

import cn.zxd.education.bean.ByteMessage;
import cn.zxd.education.bean.Device;
import cn.zxd.education.manager.DeviceManager;
import cn.zxd.education.util.ByteUtil;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@EnableAutoConfiguration
public class MainController {

    @RequestMapping("/index")
    public String index() {
        return "Index";
    }

    /**
     * 查询用户授权
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/auth", method = RequestMethod.POST)
    public String auth() {
        return null;
    }

    /**
     * 校验管理员密码
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/login")
    public String login() {
        return null;
    }

    /**
     * 获取设备列表
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/devices", method = RequestMethod.GET)
    public String getDevices() {
        return "not include";
    }

    @RequestMapping(value = "/api/v1/status", method = RequestMethod.POST)
    public String deviceStatus(@RequestParam("device") String deviceName) {
        if (deviceName == null || deviceName.length() < 1) {
            return "{\"error\":1}";
        } else {
            Device device = DeviceManager.getInstance().getDeviceByName(deviceName);
            if (device != null && device.isPowerOn()) {
                return "{\"status\":true,\"error\":0}";
            } else {
                return "{\"status\":false,\"error\":0}";
            }
        }
    }

    /**
     * 远程控制设备
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/device/control", method = RequestMethod.POST)
    public String deviceControl(@RequestParam("device") String deviceName, @RequestParam("index") int index) {
        if (deviceName == null || deviceName.length() < 1) {
            return "{\"error\":1}";
        } else {
            if (DeviceManager.getInstance().sendMessageByName(deviceName, new ByteMessage(ByteMessage.ByteMessageTypeIORequest, ByteUtil.byte2HexString(new byte[]{0x02, (byte) index}))))
                return "{\"error\":0}";
            else
                return "{\"error\":1}";
        }
    }

    /**
     * 向设备发送指定命令
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/device/cmd", method = RequestMethod.POST)
    public String deviceCmd() {
        return null;
    }

    /**
     * 获取设备授权用户
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/device/users", method = RequestMethod.GET)
    public String getDeviceAuthoredUsers() {
        return null;
    }

    /**
     * 将设备授权给用户
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/device/users", method = RequestMethod.POST)
    public String deviceAuthToUsers() {
        return null;
    }

    /**
     * 获取用户列表
     *
     * @return
     */
    @RequestMapping(value = "/api/v1/users", method = RequestMethod.GET)
    public String getUsers() {
        return null;
    }

}
