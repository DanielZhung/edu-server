package cn.zxd.education.bean;

import io.netty.channel.ChannelHandlerContext;

public class Device {

    private String registerName;

    private String nickName;

    private ChannelHandlerContext context;

    private boolean isPowerOn = false;

    private byte inputStatus;

    private byte outputStatus;

    public Device() {
    }

    public Device(String registerName, ChannelHandlerContext context) {
        this.registerName = registerName;
        this.context = context;
    }

    public String getRegisterName() {
        return registerName;
    }

    public String getNickName() {
        return nickName;
    }

    public ChannelHandlerContext getContext() {
        return context;
    }

    public byte getInputStatus() {
        return inputStatus;
    }

    public byte getOutputStatus() {
        return outputStatus;
    }

    public boolean isPowerOn() {
        return isPowerOn;
    }

    public void setPowerOn(boolean powerOn) {
        isPowerOn = powerOn;
    }
}
