package cn.zxd.education.bean;

import java.io.Serializable;

public class ByteMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int ByteMessageTypeUnknown = 0;

    public static final int ByteMessageTypeRegister = 1;

    public static final int ByteMessageTypeRegisterResponse = 2;

    public static final int ByteMessageTypeHeartBeat = 3;

    public static final int ByteMessageTypeHeartBeatResponse = 4;

    public static final int ByteMessageTypeIO = 5;

    public static final int ByteMessageTypeIORequest = 6;

    public static final int ByteMessageTypeCMD = 7;

    public static final int ByteMessageTypeCMDRequest = 8;

    public static final ByteMessage RegisterSuccess = new ByteMessage(ByteMessageTypeRegisterResponse, "00");

    public static final ByteMessage RegisterFail = new ByteMessage(ByteMessageTypeRegisterResponse, "01");

    public static final ByteMessage HeartBeatResponse = new ByteMessage(ByteMessageTypeHeartBeatResponse, "04");

    private int type;

    private String content;

    public ByteMessage() {
    }

    public ByteMessage(int type, String content) {
        this.type = type;
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
